FROM node:12

RUN npm install -g http-server

WORKDIR /app

COPY package*.json ./

COPY ./dist ./
COPY ./node_modules ./node_modules
RUN ls

EXPOSE 8080

CMD [ "http-server", "dist" ]
